---
title: "flat_first.Rmd for working package"
output: html_document
editor_options: 
  chunk_output_type: console
---

<!-- Run this 'development' chunk -->
<!-- Store every call to library() that you need to explore your functions -->

```{r development, include=FALSE}
library(testthat)
library(rvest)
library(dplyr)
library(xml2)
```

<!--
 You need to run the 'description' chunk in the '0-dev_history.Rmd' file before continuing your code there.

If it is the first time you use {fusen}, after 'description', you can directly run the last chunk of the present file with inflate() inside.
--> 

```{r development-load}
# Load already included functions if relevant
pkgload::load_all(export_all = FALSE)
```

# Télécharge les pages HTML à partir d'une URL de base

```{r function-download_html_pages}
#' @title Télécharge les pages HTML à partir d'une URL de base
#' 
#' @description Cette fonction télécharge les pages HTML à partir d'une URL de 
#' base et stocke chaque page téléchargée au format HTML dans un sous-répertoire 
#' nommé d'après la date du jour.
#'
#' @param url_base L'URL de base à partir de laquelle télécharger les pages HTML.
#' @param filepath Le chemin du répertoire dans lequel stocker les pages 
#' téléchargées.
#' @return Aucune valeur n'est renvoyée (les pages HTML sont téléchargées et
#' stockées dans le répertoire spécifié).
#' @importFrom xml2 write_html
#' @importFrom rvest html_nodes html_attr read_html
#' @importFrom dplyr bind_rows
#' @export
download_html_pages <- function(url_base, filepath) {
  # Créer un sous-répertoire nommé d'après la date du jour
  date_today <- format(Sys.Date(), "%Y%m%d")
  subfolder <- file.path(filepath, date_today)
  if (!dir.exists(subfolder)) {
    dir.create(subfolder, recursive = TRUE)
  }
  
  # Créer une liste pour stocker les données
  liste <- data.frame(titre = character(), url = character())
  
  # Fonction pour extraire le contenu de la page et télécharger
  download_page <- function(url, full_filepath) {
    page <- rvest::read_html(url)
    xml2::write_html(page, full_filepath)
  }
  
  # Télécharger la première page HTML
  lien_actif <- url_base
  download_page(lien_actif, file.path(subfolder, "page_1.html"))
  print("Page 1 t\u00e9l\u00e9charg\u00e9e avec succ\u00e8s.")
  
  # Boucler pour télécharger les pages HTML
  suite <- 0
  while (suite != 1L) {
    # Lire la page HTML
    lien_actif <- url(lien_actif, "rb")
    page <- rvest::read_html(lien_actif)
    close(lien_actif)
    
    # Extraire les titres et les URLs des annonces
    titre <- html_nodes(page, ".annLink") |>
      rvest::html_attr("title") |>
      as.character()
    url <- html_nodes(page, ".annLink") |>
      rvest::html_attr("href") |>
      as.character()
    
    # Ajouter les données à la liste
    ajout <- data.frame(titre = titre, url = url)
    liste <- dplyr::bind_rows(liste, ajout)
    
    # Récupérer le numéro de la page suivante
    num_page <- rvest::html_nodes(page, "#paginationListeKL strong") |>
      rvest::html_text() |>
      as.numeric() + 1
    
    # Vérifier s'il y a d'autres pages
    autr_pages <- rvest::html_nodes(page, "#paginationListeKL a") |>
      rvest::html_attr("data-page") |>
      as.numeric()
    suite <- length(autr_pages)
    
    # Générer le lien vers la page suivante
    lien_actif <- paste0("https://www.ouestfrance-immo.com/immobilier/location/logement/pays-de-la-loire/?page=", num_page)

    # Télécharger la page HTML dans le sous-répertoire
    full_filepath <- file.path(subfolder, paste0("page_", num_page, ".html"))
    download_page(lien_actif, full_filepath)
    print(paste("Page", num_page, "t\u00e9l\u00e9charg\u00e9e avec succ\u00e8s."))
  }
}

```

```{r examples-download_html_pages}
url_base <- "https://www.ouestfrance-immo.com/immobilier/location/logement/pays-de-la-loire/?page="
filepath <- "C:/Users/ronan.vignard/Documents/@work/@projects/SIAL/DONNEES_CLIENT/"

# Utilisation de la fonction
download_html_pages(url_base, filepath)
```

```{r development-inflate, eval=FALSE}
# Keep eval=FALSE to avoid infinite loop in case you hit the knit button
# Execute in the console directly
fusen::inflate(flat_file = "dev/flat_first.Rmd", vignette_name = "Get started")
```


# Inflate your package

You're one inflate from paper to box.
Build your package from this very Rmd using `fusen::inflate()`

- Verify your `"DESCRIPTION"` file has been updated
- Verify your function is in `"R/"` directory
- Verify your test is in `"tests/testthat/"` directory
- Verify this Rmd appears in `"vignettes/"` directory
