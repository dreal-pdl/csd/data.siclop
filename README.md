
<!-- README.md is generated from README.Rmd. Please edit that file -->

# data.siclop

<!-- badges: start -->

[![License:
MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![Lifecycle:
experimental](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://lifecycle.r-lib.org/articles/stages.html#experimental)
<!-- badges: end -->

Package de téléchargement des pages HTML du site web à partir d’une URL
de base.

## Installation

``` r
devtools::install_git("https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/data.siclop")
```

## Exemple

``` r
library(data.siclop)
download_html_pages("https://www.example.com/page=", "/path/to/directory")
```
